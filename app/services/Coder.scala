package services

import scala.annotation.tailrec

/**
  * Created by zbalvanyos on 17/05/2017.
  */
class Coder(keyword: String) {

    val abc = ('a' to 'z').foldLeft("")(_ + _)

    val extendedKey = (keyword.filter(_.isLetter) + abc).toLowerCase.distinct filter (_ != 'j') substring(0, 25)

    def getChar(x: Int, y: Int) = extendedKey((x % 5) * 5 + y % 5)

    def encode(plainText: String): String = translate(1, plainText)

    def decode(secretText: String): String = translate(4, secretText)

    def translate(offset: Int, rawText: String) = {
        val flip = flipWithOffset(offset)(_,_)

        @tailrec
        def helper(text: List[Char], result: String) : String = text match {
            case Nil => result
            case x::Nil => result + flip(x, 'z')
            case x::y::xs => {
                if(x == y) {
                    val supplementary = if(x.equals('x')) 'q' else 'x'
                    helper(y :: xs, result + flip(x, supplementary))
                } else {
                    helper(xs, result + flip(x, y))
                }
            }
        }

        val text = rawText.toLowerCase
                .replaceAll("[^0-9a-z]", "")
                .replace("j", "i")
                .toCharArray
                .toList

        format(helper(text, "")).trim
    }

    def flipWithOffset(offset: Int)(first: Char, second: Char) = {
        val firstPosition = extendedKey.indexOf(first)
        val secondPosition = extendedKey.indexOf(second)

        val (firstX, firstY) = getCoordinates(firstPosition)
        val (secondX, secondY) = getCoordinates(secondPosition)

        if(firstX == secondX) {
            getChar(firstX, firstY + offset).toString + getChar(secondX, secondY + offset).toString
        } else if (firstY == secondY) {
            getChar(firstX + offset, firstY).toString + getChar(secondX + offset, secondY).toString
        } else {
            getChar(firstX, secondY).toString + getChar(secondX, firstY).toString
        }
    }

    def getCoordinates(position: Int) = {
        val x: Int = position / 5
        val y = position % 5
        (x, y)
    }

    def format(text: String) = {

        def helper(text: String, result: String): String = text match {
            case "" => result
            case _ => {
                val nextResult = result + text.take(5)
                val suffix = if(nextResult.length % 60 == 59) System.lineSeparator() else " "
                helper(text.drop(5), nextResult + suffix)
            }
        }

        helper(text, "")
    }
}
