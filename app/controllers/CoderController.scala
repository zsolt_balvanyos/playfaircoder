package controllers

import play.api.mvc.{Controller, Action}
import services.Coder

/**
  * Created by zbalvanyos on 17/05/2017.
  */
class CoderController extends Controller {

    class TextAndKey(val text: String, val key: String)

    def encode = Action { request =>
        val text = request.body.asFormUrlEncoded.get.get("text").get.mkString
        val key = request.body.asFormUrlEncoded.get.get("key").get.mkString
        val coder = new Coder(key)
        Ok(coder.encode(text))
    }

    def decode = Action { request =>
        val text = request.body.asFormUrlEncoded.get.get("text").get.mkString
        val key = request.body.asFormUrlEncoded.get.get("key").get.mkString
        val coder = new Coder(key)
        Ok(coder.decode(text))
    }

    def welcome = Action {
         Ok(views.html.coder())
    }

}
