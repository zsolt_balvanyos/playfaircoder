$(function() {

    $('#encode').on('click', function() {

        var textAndKey = {
            text: $('#textin').val(),
            key: $('#key').val()
        };

        $.post("/encode", textAndKey, function(data) {$('#display').hide().html(data).fadeIn('1000')})

    });

    $('#decode').on('click', function() {

        var textAndKey = {
            text: $('#textin').val(),
            key: $('#key').val()
        };

        $.post("/decode", textAndKey, function(data) {$('#display').hide().html(data).fadeIn('1000')})

    });

});
