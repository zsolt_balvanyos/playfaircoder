import org.scalatest.{Matchers, FlatSpec}
import services.Coder

/**
  * Created by zbalvanyos on 21/05/2017.
  */
class CoderTest extends FlatSpec with Matchers {

    val coder = new Coder("Pennsylvania")

    "Coder" should "Select correct character" in {
        coder.getChar(1, 1) should be ('v')
        coder.getChar(5, 5) should be ('p')
    }

    "Coder" should "Show the coordinates of a position" in {
        coder.getCoordinates(6) should be (1, 1)
        coder.getCoordinates(0) should be (0, 0)
        coder.getCoordinates(4) should be (0, 4)
        coder.getCoordinates(24) should be (4, 4)
    }

    "Coder" should "Flip the character pair" in {
        coder.flipWithOffset(1)('v', 'q') should be ("im")
        coder.flipWithOffset(1)('k', 't') should be ("tp")
        coder.flipWithOffset(1)('r', 'k') should be ("km")

        coder.flipWithOffset(4)('i', 'm') should be ("vq")
        coder.flipWithOffset(4)('t', 'p') should be ("kt")
        coder.flipWithOffset(4)('k', 'm') should be ("rk")
    }

    "Coder" should "Translate text" in {
        coder.translate(1, "vqktrka") should be ("imtpk mbw")
        coder.translate(1, "vatta") should be ("aiuzw l")

        coder.translate(4, "imtpk mbw") should be ("vqktr kaz")

        val input = "An anonymous reader sends word of a proof-of-concept Google Chrome browser\nextension that steals users’ login details. The developer, Andreas Grech,\nsays that he is trying to raise awareness about security among end users,\nand therefore chose Chrome as a test-bed because of its reputation as the\nsafest browser."
        val expected = "fafaw aermw yqnvm vqyns genwm hwoln kqwow ofkpf nexcq wqfvp\ndckqu vhzwn ynmyz unsig wazcl wpxnv ipxey mpiqf asmvw lbvpx\ndymvd vaken obefm yinhq pdgyb npxfb zcsvp xzbas cxqki bynfn\nbonsn yniar wuynd tqbzp vowad sefxe ymnie fzcym ndqkp dfryn\ndckqu vinlw nyzlv mvyfl xenmg axpmy etwlx lwain zcnyf onyzl\nkqxny m"
        coder.translate(1, input) should be (expected)
    }
}
